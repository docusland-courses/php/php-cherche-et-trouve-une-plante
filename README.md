# PHP – Projet « Cherche & Trouve »
## Objectif pour l'apprenant
Mettre en œuvre les capacités acquises en cours de formation en réinvestissant ce qui a été appris en situation professionnelle en lien avec un besoin formulé.

## Modalités
Collaboration en binômes ou trinômes demandée.  

## Gestion de la communication
-   Réalisez un point de 5 minutes en début de chaque demi-journée pour définir vos objectifs d'avancement.    
-   Enregistrez un rapport d'activité, avec la répartition des tâches.
-   Centralisez vos questions techniques en fin de journée auprès des délégués

## Présentation du projet

### Objet du document
Ce document s'adresse à une équipe de développeurs d'applications web en seconde année de formation

### Descriptions des parties prenantes
-   Client / Chef de projet : Service pédagogique
-   Développeur : Etudiant en 2e année

### Objectifs

_Il s’agit d’un projet fictif, toute relation avec un projet existant ne serait que fortuite._

La ferme biologique du Bec Hellouin souhaite faire appel à vous afin de réaliser une petite application web de sensibilisation et de découverte de la flore locale. Ce projet répondant au nom de « Cherche & Trouve », souhaite mettre en place une petite application de jeu, à destination des enfants, leurs demandant de chercher autour d'eux, des plantes et de les photographier. 

Dans l'idéal, le gérant de la ferme souhaiterait en faire une application permettant de géolocaliser les plantes afin de pouvoir réutiliser ces informations au sein d'autres applications. 

### Stack technique attendue
 - Base de données : PostgreSQL ou MySQL 8
 - Technologie : PHP 8, Javascript
 - Framework : Symfony
 - Gestionnaire de templates : Twig
 - SASS

### Description

Le site doit avoir :
-   Une partie client (front end)
-   Une partie administration (back end)


#### Le Back End

La partie administration doit avoir recours à une authentification réalisée par le biais d’un login et mot de passe. Au sein de ce back end, il sera possible d’administrer l'intégralité des données.

La partie administration doit permettre l’ajout, la visualisation, la modification et la suppression de plantes.

L’interface donnera l’illusion de pouvoir supprimer une, mais en réalité la plante restera conservée en base de données.

Un filtre doit être mis en place permettant d’afficher ou non les anciennes plantes. Une plante pourra être réactivée ultérieurement.

#### Le Front End

La partie cliente est destinée avant tout à des enfants. Lorsqu'ils lancent l'application, l'application leur demande de chercher une plante dans le voisinage. 
Une fois que l'enfant a trouvé la plante en question il la prends en photo, puis il consulte un adulte afin de valider si la plante photographiée.
Si l'adulte valide, une seconde page complémentaire contenant de plus amples informations concernant la plante seront affichées. 
Puis une nouvelle plante sera proposée à l'utilisateur.
 
Les maquettes suivantes sont mises à votre disposition à titre indicatif. Il n'est pas attendu de respecter ces maquettes à la lettre:
<div style="display: flex; flex-direction: column; align-content: center;">
<img src="./maquettes/page-0.jpg" width="300px">
⌄
<img src="./maquettes/page-1.jpg" width="300px">
⌄
<img src="./maquettes/page-2.jpg" width="300px">
⌄
<img src="./maquettes/page-3.jpg" width="300px">
⌄
<img src="./maquettes/page-4.jpg" width="300px">
⌄
<img src="./maquettes/page-5.jpg" width="300px">
</div>

Vous trouverez aussi le page flow minimal attendu : 
```mermaid
graph LR
    A[Page de connexion] -->|Connexion| B(Accueil)
    B --> I[Se déconnecter]
    B --> C{Choisir une action}
    I --> A
    C -->|Jouer| D[fa:fa-play Cherche & Trouve]
    C -->|Profil| F[fa:fa-user Profil]
    C -->|Statistiques| E[fa:fa-table Stats]
    E --> |Voir| G[fa:fa-search Voir une fiche de plante]
    G --> H
    F --> H
    D --> H(Retour à l'accueil)
    H --> B

```
Afin de pouvoir jouer, l'utilisateur doit se connecter. 
Il aura alors le choix suivant :

- **Jouer**

Une première fiche de plante sera à trouver. 

Si un utilisateur parvient à trouver une plante, il gagnera des points d'expérience et pourra dorénavant chercher une nouvelle plante. 

Les plantes déjà trouvées ne seront plus proposées au sein du jeu. Les plantes proposées s'activeront en fonction du nombre du nombre d'XP du joueur. Dans un premier temps, si l'utilisateur a déjà validé trois plantes de `level` 1, l'application commencera à lui proposer des plantes de `level` 2.

- **Statistiques**

L'utilisateur pourra alors voir les fiches plantes déjà résolues. 
Une carte interactive affichant les plantes déjà géolocalisées sera affichée.
L'utilisateur aura la possibilité de cliquer sur une fiche de plante afin d'avoir plus d'informations sur cette dernière. 
Il sera possible de trier les plantes affichées en fonction du nom de la plante, mais aussi par date de résolution du jeu.


- **Modifier son profil**

Un utilisateur aura la possibilité de voir son profil. Sur cette page il verra : 
 - Le nombre de plantes trouvées
 - Son niveau d'XP
 - La possibilité de changer ses informations (mot de passe et avatar)
 - Se désinscrire

Si un utilisateur décide de se désinscrire, l'ensemble des informations liées à son compte seront également supprimées. 

### Description de l'existant

Un fichier JSON a été mis à disposition permettant d'avoir un premier jeu de données. 
Il a la structure suivante:
```
{
    "name": {"type": "string"},
    "level": {"type": "number"},
    "before": {
        "type": "array", 
        "items": {
            "properties": {
                "type": "object",
                "text": {"type": "string"},
                "logo": {"type": "string"}
            },
            "required": ["text"]
        }
    ],
    "photos" : {
        "type": "array",
        "items": {"type": "string"}
    },
    "after": {
        "type": "array",
        "items": {
            "properties": {
                "type": "object",
                "title": {"type": "string"},
                "text": {"type": "string"},
                "logo": {"type": "string"}
            },
            "required": ["text"]
        }
    },
  }
```

Vous trouverez [ici](./data/data.json), un fichier exemple de données. Un mécanisme doit avoir été mis en place afin de pouvoir importer automatiquement, lors du déploiement de l'application, l'ensemble des données issues de ce fichier.    

### Critères d'acceptabilité du produit
-   Le(s) document(s) livrés doivent être respectueux de la RGPD.
-   Le(s) document(s) livrés doivent être responsive
    

### Analyse non fonctionnelle
-   Votre application doit être accessible en français au minimum
-   L'application doit s'adapter aux différents supports informatiques (responsive)
    
### La méthodologie à appliquer

-   Etape 1 : Analyse préliminaire
    -   Questions complémentaires
    -   Définition des besoins
    -   Répartition des rôles au sein de vos équipes.
    
-   Etape 2 : Maquettage
    -   Conception des maquettes de votre application
    -   Premier jet de l’Interface en HTML et CSS

-   Etape 3 : Conception de l'UML
    -   Elaboration des modèles
    -   Définition des routes
    
-   Etape 4 : Back Office
    -   Depuis la page d’administration, pouvoir gérer les plantes, activer ou non le filtre
    -   Gestion des import de données
    
-   Etape 5 : Front Office
    -   Stylisez l'interface utilisateur
    -   Ajoutez des animations lors des transitions entre les pages. Une touche de gamification est bienvenue.
            
-   Etape 6 : Améliorations (Les améliorations compteront en tant que bonus par le jury). Que ce soit CI, CD, tests unitaires, graphismes avancés, sécurité... Mettez le en avant lors de votre présentation
  

### Les livrables du projet

Le rendu doit être réalisé sous forme d’une présentation face à un jury. Un support de présentation est attendu.
La présentation face au jury devra être technique, contenir une demonstration de l'application et présenter votre organisation ainsi que votre planification des tâches.

Un dépôt GIT est aussi à fournir, il devra contenir :
- Un fichier README.md expliquant:
- le projet et le contexte de ce sujet
- les membres ayant contribué au projet
-   Les maquettes de l’application
-   Le MCD de la base de données
-   Le code de l’application finalisé et refactoré
-   Les identifiants demo requis pour se connecter au back office .

### Conseils
-   Réfléchissez et concertez-vous avant d’avancer sur le sujet.
-   Attention à vos identifiants pour la base de données ou pour accéder à la partie back office. Ils devront être modifiables ou à défaut communiquées au formateur.
-   Le contenu que vous intégrerez devra être travaillé, soigné et professionnel.
- Travailler à plusieurs est difficile, la clé réside dans l'organisation, la communication et la répartition des tâches.


Que la force soit avec vous !
